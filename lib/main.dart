import 'package:flutter/material.dart';

import './auth/loginPage.dart';
import './auth/signupPage.dart';
import './quiz.dart';
// import './result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'What\'s your favorite colors?',
      'answers': ['Red', 'Black', 'White', 'Yellow']
    },
    {
      'questionText': 'What\'s your favorite animals?',
      'answers': ['Lion', 'Elephant', 'Monkey', 'Tiger']
    },
    {
      'questionText': 'What\'s your favorite Instructor?',
      'answers': ['Max', 'Danny', 'Jacob', 'Chapa']
    },
  ];
  var _questionIndex = 0;

  void _answerQuestions() {
    setState(() {
      _questionIndex++;
    });
    print(_questionIndex);
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(),
        '/signup': (context) => SignupPage(),
        '/home': (context) => Quiz(
            answerQuestions: _answerQuestions,
            questions: _questions,
            questionIndex: _questionIndex,
            resetQuiz: _resetQuiz),
      },
    );
  }
}
