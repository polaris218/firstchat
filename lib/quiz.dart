import 'package:flutter/material.dart';

import './question.dart';
import './answer.dart';
import './result.dart';

class Quiz extends StatelessWidget {
  final Function answerQuestions;
  final List<Map<String, Object>> questions;
  final int questionIndex;
  final Function resetQuiz;

  Quiz(
      {@required this.answerQuestions,
      @required this.questions,
      @required this.questionIndex,
      @required this.resetQuiz});

  @override
  Widget build(BuildContext context) {
    print(questionIndex);
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: questionIndex < 3
          ? Column(
              children: <Widget>[
                Question(questions[questionIndex]['questionText']),
                ...(questions[questionIndex]['answers'] as List<String>)
                    .map((answer) {
                  return Answer(() => answerQuestions(), answer);
                }).toList()
              ],
            )
          : Result(resetQuiz),
    );
  }
}
